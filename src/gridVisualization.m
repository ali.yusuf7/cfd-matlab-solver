%  This function allows visualization of streched grid, the position of
%  the cylinder in the domain and marker function values of each grid
%  point.
%
%  Copyright (C) 2019,  Syed Yusuf Ali
% 
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <https://www.gnu.org/licenses/>.
%
%  You can contact the author at syedyusufali@outlook.com

function gridVisualization(x,y,m,n,Height,L)

[XX YY] = meshgrid(x(1:n+2),y(1:m+2));

for i=1:n+1
    for j=1:m+1
    if ((XX(i,j)-2.5)^2+(YY(i,j)-(Height/2))^2) < 0.5^2
            plot(0.5*(XX(i+1,j+1)+XX(i,j)),0.5*(YY(i+1,j+1)+YY(i,j)),'.r');
            hold on;
    end
    if ((XX(i,j)-2.5)^2+(YY(i,j)-(Height/2))^2) > 0.5^2
            plot(0.5*(XX(i+1,j+1)+XX(i,j)),0.5*(YY(i+1,j+1)+YY(i,j)),'.b');
            hold on;
    end
    end
end

hold on;
plot(XX,YY,'-k');
plot(transpose(XX),transpose(YY),'-k');
daspect([1 1 1])
axis([0 L 0 Height])