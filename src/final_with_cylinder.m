%  This program is a 2D Incompressible Navier-Stokes CFD Solver written in MATLAB.
%  The program simulates the flow past a cylinder using a finite 
%  computational domain with slip Boundary conditions at top and bottom 
%  walls.
%
%  Copyright (C) 2019,  Syed Yusuf Ali
% 
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
% 
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
% 
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <https://www.gnu.org/licenses/>.
%
%  You can contact the author at syedyusufali@outlook.com

tic
clear 

gridsize = 128;
L= 10;
Height = 5;

%Run mode: gridVisualization or cylinderRun
runmode='cylinderRun';

n = gridsize;
m = gridsize;

eta=1/m; %stretching 

dx=L/n;
dy=Height/m;

dt = 0.005;
time=0;
iteration=1;
t=0;
max_iteration = 6E5;

k_max_u_star = 10000;
k_max_v_star = 10000;
k_max_p = 5E6;

Re=150;

SOR=1.99;

x = zeros(1,n+3) ;
y = zeros(1,m+3) ;

%-------video-------------------
%{
xx=1:n;
yy=1:m;
[XX YY] = meshgrid(x(2:n+1),y(2:m+1));
xxx = XX(:,:); 
yyy = YY(:,:); 

o=1;
vid=VideoWriter('vorticity.avi');
open(vid);
%}
%----------Grid Generation---------
%x(1) = -dx;
x(1) = -eta;  %stretching 
for i=1:n+2
    %x(i+1) = x(i) + dx;
    x(i+1) = x(i) + eta;  %stretching 
end

%stretching the grid

for i=1:n+3
    x(i)= L*x(i) + 2*(1.074 - L*x(i))*(1-x(i))*x(i);
end

%y(1) = -dy;
y(1) = -eta;  %stretching 
    
for j=1:m+2
    %y(j+1) = y(j) + dy;
    y(j+1) = y(j) + eta;  %stretching 
end

%stretching the grid

for j=1:m+3
    y(j)= Height*y(j) + 2*(Height/2 - Height*y(j))*(1-y(j))*y(j);
end

%---------Grid Visualization-------
if strcmp(runmode,'gridVisualization')
    gridVisualization(x,y,m,n,Height,L);
    return
end
%------Variable Initilization------
u =  zeros(n+2,m+2);
v =  zeros(n+2,m+2);
u_star =  zeros(n+2,m+2);
v_star =  zeros(n+2,m+2);
u_nm1 =  zeros(n+2,m+2);
v_nm1 =  zeros(n+2,m+2);

U_e =  zeros(n+2,m+2);
U_w =  zeros(n+2,m+2);

V_n =  zeros(n+2,m+2);
V_s =  zeros(n+2,m+2);

U_e_star =  zeros(n+2,m+2);
U_w_star =  zeros(n+2,m+2);

V_n_star =  zeros(n+2,m+2);
V_s_star =  zeros(n+2,m+2);


u_E =  zeros(n+2,m+2);
u_W =  zeros(n+2,m+2);
u_N =  zeros(n+2,m+2);
u_S =  zeros(n+2,m+2);
u_P =  zeros(n+2,m+2);

u_E_star =  zeros(n+2,m+2);
u_W_star =  zeros(n+2,m+2);
u_N_star =  zeros(n+2,m+2);
u_S_star =  zeros(n+2,m+2);
u_P_star =  zeros(n+2,m+2);

v_E_star =  zeros(n+2,m+2);
v_W_star =  zeros(n+2,m+2);
v_N_star =  zeros(n+2,m+2);
v_S_star =  zeros(n+2,m+2);
v_P_star =  zeros(n+2,m+2);

v_E =  zeros(n+2,m+2);
v_W =  zeros(n+2,m+2);
v_N =  zeros(n+2,m+2);
v_S =  zeros(n+2,m+2);
v_P =  zeros(n+2,m+2);

U_e_nm1 =  zeros(n+2,m+2);
U_w_nm1 =  zeros(n+2,m+2);

V_n_nm1 =  zeros(n+2,m+2);
V_s_nm1 =  zeros(n+2,m+2);

Du =  zeros(n+2,m+2);
Dv =  zeros(n+2,m+2);
H =  zeros(n+2,m+2);

p =  zeros(n+2,m+2);

a = zeros(n+2,m+2);  %marker

p_E =  zeros(n+2,m+2);
p_W =  zeros(n+2,m+2);
p_N =  zeros(n+2,m+2);
p_S =  zeros(n+2,m+2);
p_P =  zeros(n+2,m+2);

NLu_n =  zeros(n+2,m+2);
NLu_nm1 =  zeros(n+2,m+2);
NLv_n =  zeros(n+2,m+2);
NLv_nm1 =  zeros(n+2,m+2);


R1 = zeros(n+2,m+2);
R2 = zeros(n+2,m+2);
R3 = zeros(n+2,m+2);
Ru = zeros(1,max_iteration-1);
Rv = zeros(1,max_iteration-1);
Rp = zeros(1,10000000);
Ru_star = zeros(1,1000000);
Rv_star = zeros(1,1000000);
u_vorticity = zeros(1,1000000);
v_vorticity = zeros(1,1000000);


%-------grid parameters---------

x_E =  zeros(n+2,m+2);
x_W =  zeros(n+2,m+2);
y_N =  zeros(n+2,m+2);
y_S =  zeros(n+2,m+2);
x_P =  zeros(n+2,m+2);
y_P =  zeros(n+2,m+2);

x_e =  zeros(n+2,m+2);
x_w =  zeros(n+2,m+2);
y_n =  zeros(n+2,m+2);
y_s =  zeros(n+2,m+2);

a_E_prime =  zeros(n+2,m+2);
a_W_prime =  zeros(n+2,m+2);
a_N_prime =  zeros(n+2,m+2);
a_S_prime =  zeros(n+2,m+2);
a_P_prime =  zeros(n+2,m+2);

lambda_e = zeros(n+2,m+2);
lambda_w = zeros(n+2,m+2);
lambda_n = zeros(n+2,m+2);
lambda_s = zeros(n+2,m+2);





for i=2:n+1
    for j=2:m+1
        
        x_e(i,j)=x(i+1);
        x_w(i,j)=x(i);
        y_n(i,j)=y(j+1);
        y_s(i,j)=y(j);
        

        x_E(i,j) = 0.5*(x(i+2) + x(i+1));
        x_W(i,j) = 0.5*(x(i-1) + x(i));
        y_N(i,j) = 0.5*(y(j+2) + y(j+1));
        y_S(i,j) = 0.5*(y(j-1) + y(j));

        x_P(i,j) = 0.5*(x(i+1) + x(i));
        y_P(i,j) = 0.5*(y(j+1) + y(j));
        
        
        a_E_prime(i,j) = 1/((x_e(i,j) - x_w(i,j))*(x_E(i,j) - x_P(i,j))) ;
        a_W_prime(i,j) = 1/((x_e(i,j) - x_w(i,j))*(x_P(i,j) - x_W(i,j))) ;
        a_N_prime(i,j) = 1/((y_n(i,j) - y_s(i,j))*(y_N(i,j) - y_P(i,j))) ;
        a_S_prime(i,j) = 1/((y_n(i,j) - y_s(i,j))*(y_P(i,j) - y_S(i,j))) ;
        a_P_prime(i,j) = -(a_E_prime(i,j) + a_W_prime(i,j) + ...
            a_N_prime(i,j) + a_S_prime(i,j)) ;
    end
end



for i=2:n+1
    for j=2:m+1
        lambda_e(i,j) = (x_e(i,j) - x_P(i,j))/(x_E(i,j) - x_P(i,j));
        lambda_w(i,j) = (x_w(i,j) - x_P(i,j))/(x_W(i,j) - x_P(i,j));
        lambda_n(i,j) = (y_n(i,j) - y_P(i,j))/(y_N(i,j) - y_P(i,j));
        lambda_s(i,j) = (y_s(i,j) - y_P(i,j))/(y_S(i,j) - y_P(i,j));
    end
end

%-------marker function for Cylinder---------------


for i=2:n+1
    for j=2:m+1
        if ((x(i)-2.5)^2+(y(j)-(Height/2))^2) < 0.5^2
            a(i,j)=1/dt;
        end
    end
end




a_E =  - ((1/(2*Re))*a_E_prime) ;
a_W =  - ((1/(2*Re))*a_W_prime) ;
a_N =  - ((1/(2*Re))*a_N_prime) ;
a_S =  - ((1/(2*Re))*a_S_prime) ;
a_P = 1/dt + a  - ((1/(2*Re))*a_P_prime) ;

a_E_n =   ((1/(2*Re))*a_E_prime) ;
a_W_n =   ((1/(2*Re))*a_W_prime) ;
a_N_n =   ((1/(2*Re))*a_N_prime) ;
a_S_n =   ((1/(2*Re))*a_S_prime) ;
a_P_n = 1/dt  + ((1/(2*Re))*a_P_prime) ;




while iteration<max_iteration 
disp ('Iteration No = ');
disp(iteration);
disp('Time = ');
disp((iteration-1)*dt);

%----------Diffusion term----------
for i=2:n+1
    for j=2:m+1


        u_E(i,j) = u(i+1,j);
        u_W(i,j) = u(i-1,j);
        u_N(i,j) = u(i,j+1);
        u_S(i,j) = u(i,j-1);
        
        u_P(i,j) = u(i,j);
        
        
        

        Du(i,j) = (a_E_n(i,j)*u_E(i,j)) + (a_W_n(i,j)*u_W(i,j)) ...
            + (a_N_n(i,j)*u_N(i,j)) + (a_S_n(i,j)*u_S(i,j)) + (a_P_n(i,j)*u_P(i,j));
        
    end
end



%--------Non Linear/Convection Term---------

NLu_nm1 = NLu_n;

%......... NL for n timestep ...............
for i=2:n+1
    for j=2:m+1

        u_e = u_E(i,j)*lambda_e(i,j) + u_P(i,j)*(1-lambda_e(i,j));
        u_w = u_W(i,j)*lambda_w(i,j) + u_P(i,j)*(1-lambda_w(i,j));
        u_n = u_N(i,j)*lambda_n(i,j) + u_P(i,j)*(1-lambda_n(i,j));
        u_s = u_S(i,j)*lambda_s(i,j) + u_P(i,j)*(1-lambda_s(i,j));
        
        NLu_n(i,j)= (U_e(i,j)*u_e - U_w(i,j)*u_w)/(x_e(i,j)-x_w(i,j)) + ...
            (V_n(i,j)*u_n - V_s(i,j)*u_s)/(y_n(i,j)-y_s(i,j));
        
        
    end 
end



%......... Adam Bashforth ...............

NLu = (3/2)*NLu_n - (1/2)*NLu_nm1 ;




%----------Solving for u*-----------------


k=0;
while k< k_max_u_star


%Right BC
for q = 2:m+1
u_star(n+2,q) = u_star(n+1,q);
end
%Left BC
for q = 2:m+1
u_star(1,q) = (1/lambda_w(2,q))*(1 - (u_star(2,q))*(1-lambda_w(2,q)));
end
%Top BC 
for r = 2:n+1
u_star(r,m+2) = u_star(r,m+1);
end
%Bottom BC
for r = 2:n+1
u_star(r,1) = u_star(r,2);
end
     

for i=2:n+1
    for j=2:m+1
        
        
   
        u_E_star(i,j) = u_star(i+1,j);
        u_W_star(i,j) = u_star(i-1,j);
        u_N_star(i,j) = u_star(i,j+1);
        u_S_star(i,j) = u_star(i,j-1);
        
        u_star(i,j) = (1/a_P(i,j))*(Du(i,j) - (NLu(i,j)) - ...
            ((a_E(i,j)*u_E_star(i,j)) +(a_W(i,j)*u_W_star(i,j)) ...
            + (a_N(i,j)*u_N_star(i,j)) + (a_S(i,j)*u_S_star(i,j))));
    end
end

%--------------u_star residual-------------

for i=2:n+1
    for j=2:m+1
        
        u_E_star(i,j) = u_star(i+1,j);
        u_W_star(i,j) = u_star(i-1,j);
        u_N_star(i,j) = u_star(i,j+1);
        u_S_star(i,j) = u_star(i,j-1);

        u_P_star(i,j) = u_star(i,j);
        
        R1(i,j) = (Du(i,j) - NLu(i,j) - ((a_E(i,j)*u_E_star(i,j)) + ...
            (a_W(i,j)*u_W_star(i,j)) + (a_N(i,j)*u_N_star(i,j)) +...
            (a_S(i,j)*u_S_star(i,j))+ (a_P(i,j)*u_P_star(i,j))));
    end
end


k=k+1;



sum =0;
for i=3:n
    for j=3:m
        sum = sum + (R1(i,j))^2;   
    end
end



Ru_star(k) = sqrt(sum/(n*m));

tol = 1E-7;
if (abs(max(R1)) < tol)
    break
end

%disp(Ru_star(k))

end

%k

sum_inflow=0;
sum_outflow=0;
for j=2:m+1
    sum_outflow= sum_outflow + u_star(n+1,j)*(y_n(n+1,j)-y_s(n+1,j));
    sum_inflow= sum_inflow + (y_n(2,j)-y_s(2,j));
end

Q_correct = sum_inflow-sum_outflow;
%plot (1:k,Ru_star(1,1:k));
%figure;

   
%----- v momentum--------------

for i=2:n+1
    for j=2:m+1

        v_E(i,j) = v(i+1,j);
        v_W(i,j) = v(i-1,j);
        v_N(i,j) = v(i,j+1);
        v_S(i,j) = v(i,j-1);

        v_P(i,j) = v(i,j);
        
        
        

        Dv(i,j) = (a_E_n(i,j)*v_E(i,j)) + (a_W_n(i,j)*v_W(i,j)) + (a_N_n(i,j)*v_N(i,j)) ...
            + (a_S_n(i,j)*v_S(i,j)) + (a_P_n(i,j)*v_P(i,j));
        
    end
end



%--------Non Linear/Convection Term---------

NLv_nm1 = NLv_n;

%......... NL for n timestep ...............
for i=2:n+1
    for j=2:m+1
        
        v_e = v_E(i,j)*lambda_e(i,j) + v_P(i,j)*(1-lambda_e(i,j));
        v_w = v_W(i,j)*lambda_w(i,j) + v_P(i,j)*(1-lambda_w(i,j));
        v_n = v_N(i,j)*lambda_n(i,j) + v_P(i,j)*(1-lambda_n(i,j));
        v_s = v_S(i,j)*lambda_s(i,j) + v_P(i,j)*(1-lambda_s(i,j));
        
        NLv_n(i,j)= (U_e(i,j)*v_e - U_w(i,j)*v_w)/(x_e(i,j)-x_w(i,j)) + ...
            (V_n(i,j)*v_n - V_s(i,j)*v_s)/(y_n(i,j)-y_s(i,j));
        
        
    end 
end


%......... Adam Bashforth ...............

NLv = (3/2)*NLv_n - (1/2)*NLv_nm1 ;



%----------Solving for v*-----------------

k=0;
while k< k_max_v_star
 
%Right BC
for q = 2:m+1
v_star(n+2,q) = v_star(n+1,q);
end
%Left BC
for q = 2:m+1
v_star(1,q) = (1/lambda_w(2,q))*(0 - (v_star(2,q))*(1-lambda_w(2,q)));
end
%Top BC 
for r = 2:n+1
v_star(r,m+2) = (1/lambda_n(r,m+1))*(0 - (v_star(r,m+1))*(1-lambda_n(r,m+1)));
end
%Bottom BC
for r = 2:n+1
v_star(r,1) = (1/lambda_s(r,2))*(0 - (v_star(r,2))*(1-lambda_s(r,2)));
end


for i=2:n+1
    for j=2:m+1
        

        v_E_star(i,j) = v_star(i+1,j);
        v_W_star(i,j) = v_star(i-1,j);
        v_N_star(i,j) = v_star(i,j+1);
        v_S_star(i,j) = v_star(i,j-1);
        
        v_star(i,j) = (1/a_P(i,j))*(Dv(i,j) - (NLv(i,j)) - ((a_E(i,j)*v_E_star(i,j)) + ...
            (a_W(i,j)*v_W_star(i,j)) + (a_N(i,j)*v_N_star(i,j)) + (a_S(i,j)*v_S_star(i,j))));
    end
end

%----------v_star residual --------------------
for i=2:n+1
    for j=2:m+1
        
        v_E_star(i,j) = v_star(i+1,j);
        v_W_star(i,j) = v_star(i-1,j);
        v_N_star(i,j) = v_star(i,j+1);
        v_S_star(i,j) = v_star(i,j-1);

        v_P_star(i,j) = v_star(i,j);
        
        R2(i,j) = (Dv(i,j) - NLv(i,j) - ((a_E(i,j)*v_E_star(i,j)) + ...
            (a_W(i,j)*v_W_star(i,j)) + (a_N(i,j)*v_N_star(i,j)) + (a_S(i,j)*v_S_star(i,j))...
            + (a_P(i,j)*v_P_star(i,j))));
    end
end

k=k+1;




sum =0;
for i=1:n
    for j=1:m
        sum = sum + (R2(i,j))^2;   
    end
end

Rv_star(1,k) = sqrt(sum/(n*m));

tol = 1E-7;
if (abs(max(R2)) < tol)
    break
end

end

k

%plot (1:k,Rv_star(1,1:k));
%figure



%--------Pressure Poisson------------------


% ..........Calculate RHS of PPE ..........

for i=2:n+1
    for j=2:m+1
        
%
        u_E_star(i,j) = u_star(i+1,j);
        u_W_star(i,j) = u_star(i-1,j);

        v_N_star(i,j) = v_star(i,j+1);
        v_S_star(i,j) = v_star(i,j-1);

        u_P_star(i,j) = u_star(i,j);
        v_P_star(i,j) = v_star(i,j);
        
%}
        
%======Should I enforce BC again?========        
        
        U_e_star(i,j) = u_E_star(i,j)*lambda_e(i,j) + u_P_star(i,j)*(1-lambda_e(i,j));
        U_w_star(i,j) = u_W_star(i,j)*lambda_w(i,j) + u_P_star(i,j)*(1-lambda_w(i,j));
        V_n_star(i,j) = v_N_star(i,j)*lambda_n(i,j) + v_P_star(i,j)*(1-lambda_n(i,j));
        V_s_star(i,j) = v_S_star(i,j)*lambda_s(i,j) + v_P_star(i,j)*(1-lambda_s(i,j));
        
        H(i,j) = (1/dt)*((U_e_star(i,j) - U_w_star(i,j))/(x_e(i,j) - x_w(i,j)) +...
            (V_n_star(i,j) - V_s_star(i,j))/(y_n(i,j) - y_s(i,j)));
            
        
    end
end


k=0;
while k< k_max_p
 
p_old=p;


%
for i=2:n+1
    for j=2:m+1
        
        p_E(i,j) = p(i+1,j);
        p_W(i,j) = p(i-1,j);
        p_N(i,j) = p(i,j+1);
        p_S(i,j) = p(i,j-1);
        
         
        p(i,j) = (1-SOR)*p(i,j) + SOR*(1/a_P_prime(i,j))*(H(i,j) - ((a_E_prime(i,j)*p_E(i,j)) + ...
            (a_W_prime(i,j)*p_W(i,j)) + (a_N_prime(i,j)*p_N(i,j)) + (a_S_prime(i,j)*p_S(i,j))));
    end
end

%Pressure BC
p(2:n+1,1) = p(2:n+1,2);
p(2:n+1,m+2) = p(2:n+1,m+1);
p(1,2:m+1) = p(2,2:m+1);
%p(n+2,2:m+1) = p(n+1,2:m+1);

for q = 2:m+1
%p(n+2,q) = (1/lambda_e(n+1,q))*((-Q_correct/(sum_inflow*dt)) - (p(n+1,q))*(1-lambda_e(n+1,q)));
p(n+2,q) = p(n+1,q) + (x_E(n+1,q) - x_P(n+1,q))*(-Q_correct/(sum_inflow*dt));
end




for i=2:n+1
    for j=2:m+1
        
        p_E(i,j) = p(i+1,j);
        p_W(i,j) = p(i-1,j);
        p_N(i,j) = p(i,j+1);
        p_S(i,j) = p(i,j-1);
        
        p_P(i,j) = p(i,j);
        
        R3(i,j) = H(i,j) - ((a_E_prime(i,j)*p_E(i,j)) + ...
            (a_W_prime(i,j)*p_W(i,j)) + (a_N_prime(i,j)*p_N(i,j)) + (a_S_prime(i,j)*p_S(i,j)) ...
            + (a_P_prime(i,j)*p_P(i,j)));
    end
end

k=k+1;




sum =0;
for i=1:n
    for j=1:m
        sum = sum + (R3(i,j))^2;   
    end
end


Rp(k) = sqrt(sum/(n*m));

tol =1E-6;
if (abs(max(R3)) < tol)
    break
end
%}



end

%k

%loglog (1:k,Rp(1,1:k));
%figure




%-----------updating final velocities--------

u_nm1 = u;
v_nm1 = v;

U_e_nm1 = U_e;
U_w_nm1 = U_w;

V_n_nm1 = V_n;
V_s_nm1 = V_s;


for i=2:n+1
    for j=2:m+1
        
        u(i,j) = u_star(i,j) - (dt*((p_E(i,j)-p_W(i,j))/(x_E(i,j)-x_W(i,j))));
        v(i,j) = v_star(i,j) - (dt*((p_N(i,j)-p_S(i,j))/(y_N(i,j)-y_S(i,j))));
        
        U_e(i,j) = U_e_star(i,j) - (dt*((p_E(i,j)-p_P(i,j))/(x_E(i,j)-x_P(i,j))));
        U_w(i,j) = U_w_star(i,j) - (dt*((p_P(i,j)-p_W(i,j))/(x_P(i,j)-x_W(i,j))));
        
        V_n(i,j) = V_n_star(i,j) - (dt*((p_N(i,j)-p_P(i,j))/(y_N(i,j)-y_P(i,j))));
        V_s(i,j) = V_s_star(i,j) - (dt*((p_P(i,j)-p_S(i,j))/(y_P(i,j)-y_S(i,j)))); 

    end
end
 

%---------------Boundary Conditions-----------------
%Right BC
for q = 2:m+1
u(n+2,q) = u(n+1,q);
end
%Left BC
for q = 2:m+1
u(1,q) = (1/lambda_w(2,q))*(1 - (u(2,q))*(1-lambda_w(2,q)));
end
%Top BC 
for r = 2:n+1
u(r,m+2) = u(r,m+1);
end
%Bottom BC
for r = 2:n+1
u(r,1) = u(r,2);
end

%Right BC
for j = 2:m+1
v(n+2,j) = v(n+1,j);
end
%Left BC
for j = 2:m+1
v(1,j) = (1/lambda_w(2,j))*(0 - (v(2,j))*(1-lambda_w(2,j)));
end
%Top BC 
for i = 2:n+1
v(i,m+2) = (1/lambda_n(i,m+1))*(0 - (v(i,m+1))*(1-lambda_n(i,m+1)));
end
%Bottom BC
for i = 2:n+1
v(i,1) = (1/lambda_s(i,2))*(0 - (v(i,2))*(1-lambda_s(i,2)));
end

u_final_err= abs(u-u_nm1);





%------Residual Calculation---------------
sum =0;
for i=2:n+1
    for j=2:m+1
        sum = sum + (u(i,j)-u_nm1(i,j))^2;
    end
end

Ru(iteration) = sqrt(sum/(n*m));    %u residual

sum =0;
for i=2:n+1
    for j=2:m+1
        sum = sum + (v(i,j)-v_nm1(i,j))^2;   
    end
end

Rv(iteration) = sqrt(sum/(n*m));   %v residual



tol = 1E-6;
if (Ru(iteration) < tol) && (Rv(iteration) < tol)
    flag =9
    break
end

%velcity save 

u_vorticity(iteration)= u(floor(0.4*n),m/2);
v_vorticity(iteration)= v(floor(0.4*n),m/2);



iteration  = iteration + 1;




%{
%video capture
 xx=1:n;
 yy=1:m;
 [XX,YY] = meshgrid(x(2:n+1),y(2:m+1));
 
 
 
xxx = XX(:,:); 
yyy = YY(:,:); 
uuu = transpose(u(2:n+1,2:m+1)); 
vvv = transpose(v(2:n+1,2:m+1)); 
cav = curl(xxx,yyy,uuu,vvv);
pcolor(xxx,yyy,cav);
shading interp
%hold on
%quiver(xxx,yyy,uuu,vvv,'k');
colormap('HSV');
daspect([1 1 1])
colorbar

M(o) = getframe(figure(1));
o=o+1;
%}


%------data collection------
time = (iteration-1)*dt;
%time_record=(time*10);
%if (time_record==floor(time_record))
if (mod(time,1)==0)
    
    [XX YY] = meshgrid(x(2:n+1),y(2:m+1));
    starty=y(2:3:m+1);
    startx = ones(size(starty));
    startx=0.4*startx;
    streamline(XX,YY,transpose(u(2:n+1,2:m+1)),transpose(v(2:n+1,2:m+1)),startx,starty);
    hold on;

    startx = ones(size(starty));
    startx=4.0*startx;
    streamline(XX,YY,transpose(u(2:n+1,2:m+1)),transpose(v(2:n+1,2:m+1)),startx,starty);
 
    starty=y(2:3:m+1);
    startx = ones(size(starty));
    startx=5.5*startx;
    streamline(XX,YY,transpose(u(2:n+1,2:m+1)),transpose(v(2:n+1,2:m+1)),startx,starty);
    
    starty=y(2:3:m+1);
    startx = ones(size(starty));
    startx=8.0*startx;
    streamline(XX,YY,transpose(u(2:n+1,2:m+1)),transpose(v(2:n+1,2:m+1)),startx,starty);
    
    daspect([1 1 1])
    saveas(gcf,['output/stream_',num2str(time),'.fig'])
    close(gcf);
    
    contourf(x(2:n+1),y(2:m+1),transpose(p(2:n+1,2:m+1)));
    daspect([1 1 1])
    saveas(gcf,['output/p_',num2str(time),'.fig'])
    close(gcf);
    
    contourf(x(2:n+1),y(2:m+1),transpose(u(2:n+1,2:m+1)),12);
    daspect([1 1 1])
    saveas(gcf,['output/u_',num2str(time),'.fig'])
    close(gcf);
    
    contourf(x(2:n+1),y(2:m+1),transpose(v(2:n+1,2:m+1)));
    daspect([1 1 1])
    saveas(gcf,['output/v_',num2str(time),'.fig'])
    close(gcf);
    
    [XX,YY] = meshgrid(x(2:n+1),y(2:m+1)); 
    xxx = XX(:,:); 
    yyy = YY(:,:); 
    uuu = transpose(u(2:n+1,2:m+1)); 
    vvv = transpose(v(2:n+1,2:m+1)); 
    cav = curl(xxx,yyy,uuu,vvv);
    pcolor(xxx,yyy,cav);
    shading interp
%hold on
%quiver(xxx,yyy,uuu,vvv,'k');
    colormap('HSV');
    daspect([1 1 1])
    colorbar
    saveas(gcf,['output/vor_',num2str(time),'.fig'])
    close(gcf);
end
end

%video capture

%writeVideo(vid,M);
%close(vid);

loglog (1:(iteration-1),Ru(1:(iteration-1)));
grid on;
figure;
loglog (1:(iteration-1),Rv(1:(iteration-1)));
grid on;

toc
